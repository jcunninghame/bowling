from unittest import TestCase

from bowling import Game


class BowlingTest(TestCase):

    def setUp(self):
        self._game = Game.Game()

    def test_shouldReturnAScoreOfZeroAfterNoRolls(self):
        self._game.roll(0)

        score = self._game.score()

        self.assertEqual(0, score)

    def test_shouldReturnAScoreOfOneAfterOnePinIsKnockedDown(self):
        self._game.roll(1)

        score = self._game.score()

        self.assertEqual(1, score)

    def test_shouldReturnAScoreOfTwoAfterTwoPinsAreKnockedDown(self):
        self._game.roll(2)

        score = self._game.score()

        self.assertEqual(2, score)

    def test_shouldReturnAScoreOf8AfterTwoPinsThen6PinsAreKnockedDown(self):
        self._game.roll(2)
        self._game.roll(6)

        score = self._game.score()

        self.assertEqual(8, score)

    def test_shouldReturnAScoreOf20AfterScoringASpareTheBowling5(self):
        self._game.roll(5)
        self._game.roll(5)
        self._game.roll(5)

        score = self._game.score()

        self.assertEqual(20, score)

    def test_shouldReturnAScoreOf18AfterScoringAStrikeTheBowling1PinsThen3Pins(self):
        self._game.roll(10)
        self._game.roll(1)
        self._game.roll(3)

        score = self._game.score()

        self.assertEqual(18, score)

    def test_shouldReturnAScoreOf16AfterScoringA4PinsForFourConsecutiveBowls(self):
        self._game.roll(4)
        self._game.roll(4)
        self._game.roll(4)
        self._game.roll(4)

        score = self._game.score()

        self.assertEqual(16, score)

    def test_shouldReturnAScoreOf38AfterScoringASpareThenAStrikeThen4Pins(self):
        self._game.roll(4)
        self._game.roll(6)
        self._game.roll(10)
        self._game.roll(4)

        score = self._game.score()

        self.assertEqual(38, score)

    def test_shouldReturnAScoreOf300AfterAPerfectGame(self):
        self._game.roll(10)
        self._game.roll(10)
        self._game.roll(10)
        self._game.roll(10)
        self._game.roll(10)
        self._game.roll(10)
        self._game.roll(10)
        self._game.roll(10)
        self._game.roll(10)
        self._game.roll(10)
        self._game.roll(10)

        score = self._game.score()

        self.assertEqual(300, score)
