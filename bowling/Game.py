"""
' Class representing a 10-pin bowling game
'
"""


def f_iter(frame):
    while frame is not None:
        yield frame
        frame = frame.next_frame


def flat(array):
    return [item for sublist in array for item in sublist]


class Game:
    def __init__(self):
        self._first_frame = Frame([])

    def roll(self, pins):
        self._first_frame.roll(pins)

    def score(self):
        return sum(map(lambda f: f.score(), self.all_frames()))

    def all_frames(self):
        return f_iter(self._first_frame)


class Frame:
    def __init__(self, rolls):
        self.rolls = rolls
        self.next_frame = None

    def roll(self, pins):
        if self.is_complete():
            if self.next_frame is None:
                self.next_frame = Frame([])
            self.next_frame.roll(pins)
        else:
            self.rolls.append(pins)

    def is_complete(self):
        return len(self.rolls) == 2 or sum(self.rolls) == 10

    def score(self):
        if self.is_strike():
            return sum(self.rolls) + self.get_next_rolls(2)
        elif self.is_spare():
            return sum(self.rolls) + self.get_next_rolls(1)
        else:
            return sum(self.rolls)

    def get_next_rolls(self, x):
        return sum(flat(map(lambda f: f.rolls, f_iter(self.next_frame)))[:x])

    def is_strike(self):
        return len(self.rolls) == 1 and sum(self.rolls) == 10

    def is_spare(self):
        return len(self.rolls) == 2 and sum(self.rolls) == 10
